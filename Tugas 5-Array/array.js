//Soal 1 Range
var array1 = []; 
function range(startNum, finishNum) {
  if(startNum<finishNum){
    for(i=startNum; i <= finishNum; i++){
      array1.push(i);
    }
  }   
  else if(startNum>finishNum){
    for(i=startNum; i >= finishNum; i--){
      array1.push(i);
    }
  }else{
    array1.push(-1);
  }
  return array1;
}
console.log(range(1,10));

//Soal 2 Range with Step
var array2 = [];
function rangeWithStep(startNum, finishNum, step) {
  if(startNum<finishNum){
    for(i=startNum; i <= finishNum; i+=step){
      array2.push(i);
    }
  }   
  else if(startNum>finishNum){
    for(i=startNum; i >= finishNum; i-=step){
      array2.push(i);
    }
  }else{
    array2.push(-1);
  }
  return array2;
}
console.log(rangeWithStep(1, 10, 2));

//Soal 3 Sum of Range
var array = [];
var jumlah = 0;
function sum (startNum, finishNum, step){
if(finishNum != null && startNum != null){
  if(startNum<finishNum && step != null){
    for(i=startNum; i <= finishNum; i+=step){
      array.push(i);
    }
  }   
  else if(startNum>finishNum && step != null){
    for(i=startNum; i >= finishNum; i-=step){
      array.push(i);
    }
  }else if(step = true) {
    if(startNum<finishNum){
      for(i=startNum; i <= finishNum; i++){
        array.push(i);
      }
    }   
    else if(startNum>finishNum){
      for(i=startNum; i >= finishNum; i--){
        array.push(i);
      }
    }
  }

  for(x=0; x <array.length ;x++){
    jumlah += array[x];
  } 

  return jumlah;

}else if(finishNum = true && startNum != null){
  return startNum;
}else {
  return 0;
}
}

console.log(sum(20,10,2));

//Soal 4 Array Multidimensi
var input = [
  ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
  ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
  ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
  ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
]

function dataHandling(){
for(i=0; i<input.length; i++){
console.log("Nomor ID: " + input[i][0]);
console.log("Nama Lengkap: " + input[i][1]);
console.log("TTL: " + input[i][2] +" "+ input[i][3]);
console.log("Hobi: " + input[i][4]);
console.log("");
}
}

dataHandling();

//Soal 5 Balik Kata
function balikKata(kata){
  var   kumpulanHuruf = kata;
  var arrayHuruf = [];
  var reverseArray= [];
  var newWord = "";
  for(i=0; i<kumpulanHuruf.length; i++){
    arrayHuruf.push(kumpulanHuruf[i]);
  }
  for(x=arrayHuruf.length-1; x>=0 ; x--){
    reverseArray.push(arrayHuruf[x]);
  }
  for(y=0; y<reverseArray.length; y++){
    newWord += reverseArray[y];
  }
    return newWord;
  }
  console.log(balikKata("Haji Ijah"));

//Soal 6 Metode Array
var input = ["0001", "Roman Alamsyah ", "Bandar Lampung", "21/05/1989", "Membaca"];

var newDate;
var newDateFormat;
var dateStripFormat;
var nama;
function dataHandling2(data){
  
  input.splice(1,1,"Roman Alamsyah Elsharawy");
  input.splice(2,1,"Provinsi Bandar Lampung");
  input.splice(4,1,"Pria","SMA Internasional Metro");
  
  console.log(input);

  newDate = input[3].split("/");
  switch (newDate[1]){
    case '01': console.log("Januari"); break;
    case '02': console.log("Februari"); break;
    case '03': console.log("Maret"); break;
    case '04': console.log("April"); break;
    case '05': console.log("Mei"); break;
    case '06': console.log("Juni"); break;
    case '07': console.log("Juli"); break;
    case '08': console.log("Agustus"); break;
    case '09': console.log("September"); break;
    case '10': console.log("Oktober"); break;
    case '11': console.log("November"); break;
    case '12': console.log("Desember"); break;
  }

  newDate.sort(function(a, b){return b-a});
  console.log(newDate);

  newDateFormat=input[3].split("/");
  dateStripFormat = newDateFormat.join("-");
  console.log(dateStripFormat);

  nama = input[1].slice(0,14);
  console.log(nama);
  
}

 dataHandling2();