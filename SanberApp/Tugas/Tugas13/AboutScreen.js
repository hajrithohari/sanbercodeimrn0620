import React, { Component } from 'react'
import { View, StyleSheet, Text, Image, TouchableOpacity, FlatList } from 'react-native'
import { AntDesign } from '@expo/vector-icons'; 

export default class About extends Component {
    render(){
        return(
            <View style={styles.container}>
                <View style={styles.tentangSaya}>
                    <Text style={styles.header}>Tentang Saya</Text>
                    <Image source={require('./Images/anonymous-user.png')} style={{width:150, height:150, borderRadius:150, top:15}} />
                    <Text style={styles.nama}>Joe Henry</Text>
                    <Text style={styles.keterangan}>React Native Developer</Text>
                </View>
                <View style={styles.box}>
                    <Text style={styles.subHeading}>Portofolio</Text>
                    <View style={styles.boxPorto}>
                        <View style={styles.ikon}>
                            <AntDesign name="gitlab" size={40} color="#3EC6FF"/>
                            <Text style={styles.textAkun}>@joehenry</Text>
                        </View>
                        <View style={styles.ikon}>
                            <AntDesign name="github" size={40} color="#3EC6FF" />
                            <Text style={styles.textAkun}>@joehenry</Text>
                        </View>
                    </View>
                </View>
                <View style={{height:10}}/>
                <View style={styles.box2}>
                    <Text style={styles.subHeading}>Hubungi Saya</Text>
                        <View>
                            <View style={styles.ikon}>
                                <AntDesign name="facebook-square" size={40} color="#3EC6FF"/>
                                <Text style={styles.textAkun}>@joe.henry</Text>
                            </View>
                            <View style={styles.ikon}>
                                <AntDesign name="instagram" size={40} color="#3EC6FF" />
                                <Text style={styles.textAkun}>@joehenry</Text>
                            </View>
                            <View style={styles.ikon}>
                                <AntDesign name="twitter" size={40} color="#3EC6FF" />
                                <Text style={styles.textAkun}>@joehenry</Text>
                            </View>
                        </View>    
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container:{
        flex: 1,
        paddingTop: 30,
        alignItems: 'center'
    },
    tentangSaya:{
        alignItems: 'center',
        justifyContent: 'center',
        paddingTop: 15
    },
    header:{
        fontFamily: 'Roboto',
        fontWeight: 'bold',
        fontSize: 36,
        lineHeight: 42,
        color: '#003366'
    },
    nama:{
        top: 20,
        fontFamily: 'Roboto',
        fontWeight: 'bold',
        fontSize: 24,
        lineHeight: 28,
        color: '#003366'
    },
    keterangan:{
        top: 20,
        fontFamily: 'Roboto',
        fontWeight: 'bold',
        fontSize: 16,
        lineHeight: 28,
        color: '#3EC6FF'
    },
    box:{
        justifyContent: 'center',
        top: 25,
        width: 359,
        height: 100,
        backgroundColor: '#EFEFEF',
        borderRadius: 16,
        paddingHorizontal: 15
    },
    box2:{
        justifyContent: 'center',
        top: 25,
        width: 359,
        height: 240,
        backgroundColor: '#EFEFEF',
        borderRadius: 16,
        paddingHorizontal: 15
    },
    boxPorto:{
        flexDirection: 'row',
        justifyContent: 'space-around'
    },
    ikon:{
        justifyContent: 'center',
        alignItems: 'center'
    },
    subHeading:{
        paddingBottom: 3,
        marginBottom: 2,
        fontFamily: 'Roboto',
        fontWeight: 'bold',
        fontSize: 18,
        lineHeight: 21,
        color: '#003366',
        borderBottomWidth: 1,
    },
    textAkun:{
        fontFamily: 'Roboto',
        fontWeight: 'bold',
        fontSize: 16,
        lineHeight: 28,
        color: '#003366'
    }
})