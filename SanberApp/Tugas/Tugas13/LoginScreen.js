import React, { Component } from 'react'
import { View, StyleSheet, Text, Image, TouchableOpacity, FlatList } from 'react-native'

export default class Login extends Component {
    render(){
        return(
            <View style={styles.Container}>
                <View style={styles.appLogo}>
                    <Image source={require('./Images/logo.png')} style={{width:375, height:102}} />
                    <Text style={styles.loginText}>Login</Text>
                </View>
                <View style={styles.loginBox}>
                    <View style={styles.box}>
                        <Text style={styles.userInput}>Username/Email</Text>
                    </View>
                    <View style={{width: 294,height: 48}} />
                    <View style={styles.box}>
                        <Text style={styles.userInput}>Password</Text>
                    </View>
                </View>
                <View style={styles.buttonBox}>
                    <View style={styles.tombolMasuk}>
                        <Text style={styles.textTombol}>Masuk</Text>
                    </View>
                    <View style={{justifyContent: 'center', alignItems:'center', width: 140, height: 40}}>
                        <Text style={{fontFamily: 'Roboto', fontSize: 24, color:'#3EC6FF'}}>atau</Text>
                    </View>
                    <View style={styles.tombolDaftar}>
                        <Text style={styles.textTombol}>Daftar?</Text>
                    </View>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    Container:{
        flex: 1,
        paddingTop: 30
    },
    appLogo:{
        justifyContent: 'center',
        alignItems: 'center',
        paddingTop: 33,
        paddingBottom: 50
    },
    loginText:{
        top: 50,
        fontSize: 24,
        lineHeight: 28,
        color: '#003366',
        fontFamily: 'Roboto'   
    },
    loginBox:{
        justifyContent:'center',
        alignItems: 'center',
        paddingTop: 30,
    },
    userInput:{
        bottom: 20,
        alignSelf: 'flex-start',
        fontFamily: 'Roboto',
        fontSize: 16,
        lineHeight: 19,
        color: '#003366'
    },
    box:{
        width: 294,
        height: 48,
        borderWidth: 1,
        borderColor: '#003366'
    },
    buttonBox:{
        justifyContent:'center',
        alignItems: 'center',
        paddingTop: 60
    },
    tombolMasuk:{
        justifyContent: 'center',
        alignItems: 'center',
        width: 140,
        height: 40,
        backgroundColor: '#3EC6FF',
        borderRadius: 16
    },
    tombolDaftar:{
        justifyContent: 'center',
        alignItems: 'center',
        width: 140,
        height: 40,
        backgroundColor: '#003366',
        borderRadius: 16
    },
    textTombol:{
        fontFamily: 'Roboto',
        fontSize: 24,
        lineHeight: 28,
        color: '#FFFFFF'
    }
})