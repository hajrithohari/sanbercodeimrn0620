//Soal 1 Looping while
console.log("LOOPING PERTAMA");
var number = 2;
while(number < 22 ){
  console.log (number + " - I love coding");
  number += 2 ;
}
console.log("LOOPING KEDUA")
var number2 = 20;
while(number2 > 0){
  console.log (number2 + " - I will become a mobile developer");
  number2 -= 2;
}

//Soal 2 Looping menggunakan for
var kata = ["Santai", "Berkualitas", "I love coding"]
console.log("OUTPUT");
for (angka = 1; angka < 21; angka++){
 if(angka % 2 === 0){
   console.log(angka +" - "+ kata[1]);  
 }else if(angka % 2 !== 0 && angka % 3 === 0){
   console.log(angka +" - "+ kata[2]);
 }else{console.log(angka +" - "+ kata[0]);}
}

//Soal 3 Membuat Persegi Panjang #
for (i = 1; i < 5; i++){
    console.log("########");
}

//Soal 4 Membuat Tangga 
var box = "";
for(i=1; i<8; i++){
  box += "#";
  console.log(box);
}

//Soal 5 Membuat Papan Catur
for(i=1; i<9; i++){
    if(i % 2 === 0){
       console.log("# # # # ");
      }
    else{
      console.log(" # # # #");
    }
  }