//Soal 1 Array to Object
var people = [ ["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"] ]

function arrayToObject(data) {
    
    var arr = data;

    for(i=0; i<arr.length; i++){
    function userAge(){
      var now = new Date();
      var thisYear = now.getFullYear(); 
      if(arr[i][3]<thisYear){
        return thisYear-arr[i][3];
      }else if(arr[i][3]>thisYear){
        return "Invalid Birth Year";
      }else{
        return "Invalid Birth Year";
      }
    }  
    var user = i+1+". "+arr[i][0]+" "+arr[i][1]+" : ";
    var newObject = {
      firstName : arr[i][0],
      lastName: arr[i][1],
      gender: arr[i][2],
      age: userAge()
    }
    console.log(user); 
    console.log(newObject); 
    }    
}

arrayToObject(people) ;

//Soal 2 Shopping Time
var harga = [50000, 175000, 250000, 500000, 1500000]
var barang = ["Casing Handphone", "Sweater Uniklooh", "Baju H&N", "Baju Zoro","Sepatu Stacattu"]
  
function shoppingTime(_memberId, _money) {
  function item(_money){
    var itemPurchased = [];
  
    for(x=4; x>=0; x--){
      if(_money >= harga[x]){
        _money -= harga[x];
        itemPurchased.push(barang[x]);
      }
    }
    return itemPurchased;    
  }

  function kembalian(_money){
  
    for(y=4; y>=0; y--){
      if(_money >= harga[y]){
        _money -= harga[y];
      }
    }
    return _money;
  }

  var pelanggan = {
    memberId: _memberId,
    money: _money,
    listPurchased: item(_money),
    changeMoney: kembalian(_money)
  }
  
  if(_memberId !== '' && _money >= 50000){

    return pelanggan;  

  }else if(_memberId !== '' && _money < 50000){

    return "Mohon maaf, uang tidak cukup";

  }else{
    
    return "Mohon maaf, toko X hanya berlaku untuk member saja";

  }
}

console.log(shoppingTime('1820RzKrnWn08', 2475000));
  //{ memberId: '1820RzKrnWn08',
  // money: 2475000,
  // listPurchased:
  //  [ 'Sepatu Stacattu',
  //    'Baju Zoro',
  //    'Baju H&N',
  //    'Sweater Uniklooh',
  //    'Casing Handphone' ],
  // changeMoney: 0 }
console.log(shoppingTime('82Ku8Ma742', 170000));
//{ memberId: '82Ku8Ma742',
// money: 170000,
// listPurchased:
//  [ 'Casing Handphone' ],
// changeMoney: 120000 }
console.log(shoppingTime('',2475000)); //Mohon maaf, toko X hanya berlaku untuk member saja
console.log(shoppingTime('234JdhweRxa53',15000)); //Mohon maaf, uang tidak cukup
console.log(shoppingTime()); ////Mohon maaf, toko X hanya berlaku untuk member saja

//Soal 3 Naik Angkot
function naikAngkot(arrPenumpang) {

  var input = arrPenumpang;
  var output = [];

  for(i=0; i<input.length; i++){

    function tarif(ongkos){
      rute = ["A", "B", "C", "D", "E", "F"];
      var awal = input[i][1];
      var akhir = input[i][2];
      for(x=0; x<6; x++){
        if(awal == rute[x] && akhir == rute[x+1]){
          return 2000;    
        }else if(awal == rute[x] && akhir == rute[x+2]){
          return 4000;
        }else if(awal == rute[x] && akhir == rute[x+3]){
          return 6000;
        }else if(awal == rute[x] && akhir == rute[x+4]){
          return 8000;
        }else if(awal == rute[x] && akhir == rute[x+5]){
          return 10000;
        }
      }
    }

    var pengguna = {
      penumpang: input[i][0],
      naikDari: input[i][1],
      tujuan: input[i][2],
      bayar: tarif()
    }  

    output.push(pengguna);
  
  }

  return output;

}

console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
console.log(naikAngkot([])); //[]
