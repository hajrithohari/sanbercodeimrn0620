// di index.js
var readBooks = require('./callback.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]
 
// Tulis code untuk memanggil function readBooks di sini
function membaca (time, index){
  readBooks(time, books[index], function (sisaWaktu){
      readBooks(sisaWaktu, books[index+1], function(sisaWaktu){
        readBooks(sisaWaktu, books[index+2])
      })
    })
}

membaca(10000, 0)