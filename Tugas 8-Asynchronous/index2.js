var readBooksPromise = require('./promise.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]
 
// Lanjutkan code untuk menjalankan function readBooksPromise 
function baca (time, index){
readBooksPromise(time, books[index])
.then(function(fulfilled){
  readBooksPromise(fulfilled, books[index+1])
  .then(function(fulfilled){
    readBooksPromise(fulfilled, books[index+2])
  })
})
.catch(function (sisaWaktu) {
    
  console.log(sisaWaktu);
       
 });
}

baca(10000, 0)