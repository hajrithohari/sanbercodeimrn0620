//Soal 1

var nama = ["John", "Jane", "Jenita", "Junaedi"];
var peran = ["Penyihir", "Guard", "Werewolf"];

// Output untuk Input nama = '' dan peran = ''
if(nama === '' && peran === ''){
    console.log("Nama harus diisi!");
}
 
//Output untuk Input nama = 'John' dan peran = ''
else if(nama === 'John' && peran === ''){
    console.log("Halo John, Pilih peranmu untuk memulai game!");
}
 
//Output untuk Input nama = 'Jane' dan peran 'Penyihir'
else if(nama === 'Jane' && peran === 'Penyihir'){
    console.log("Selamat datang di Dunia Werewolf, Jane");
    console.log("Halo Penyihir Jane, kamu dapat melihat siapa yang menjadi werewolf!")
}
 
//Output untuk Input nama = 'Jenita' dan peran 'Guard'
else if(nama === 'Jenita' && peran === 'Guard'){
    console.log("Selamat datang di Dunia Werewolf, Jenita");
    console.log("Halo Guard Jenita, kamu akan membantu melindungi temanmu dari serangan werewolf.");
}

//Output untuk Input nama = 'Junaedi' dan peran 'Werewolf'
else if(nama === 'Junaedi' && peran === 'Werewolf'){
    console.log("Selamat datang di Dunia Werewolf, Junaedi");
    console.log("Halo Werewolf Junaedi, Kamu akan memakan mangsa setiap malam!");
}

//Soal 2
var tanggal = 1;
var bulan =  1 ;
var tahun = 2200;

var namaBulan = ["Undefined", "Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"];

if (tahun <= 1900 || tahun >= 2200){
console.log("Tahun Salah!");
}
else{
switch(tanggal){
  case 1:
  case 2:
  case 3:
  case 4:
  case 5:
  case 6:
  case 7:
  case 8:
  case 9:
  case 10:
  case 11:
  case 12:
  case 13:
  case 14:
  case 15:
  case 16:
  case 17:
  case 18:
  case 19:
  case 20:
  case 21:
  case 22:
  case 23:
  case 24:
  case 25:
  case 26:
  case 27:
  case 28:
  case 29:
  case 30:
  case 31:
  {console.log(tanggal + ' ' + namaBulan[bulan] + ' ' + tahun); break;}
default: {console.log("Tanggal Salah!");}
}
}

