//Soal 1
class Animal {
    constructor (name){
      this.name = name ;
      this.cold_blooded = false;
      this.legs = 4
    }
    get kaki(){
      return this.legs;
    }
    get darah(){
      return this.cold_blooded;
    }
}
 
var sheep = new Animal("shaun");
 
console.log(sheep.name) // "shaun"
console.log(sheep.legs) // 4
console.log(sheep.cold_blooded) // false

class Ape extends Animal {
  constructor (name){
    super (name);
    this.legs = 2;
  }

  yell(){
    return "Auooo";
  } 

}

var sungokong = new Ape("kera sakti");


class Frog extends Animal {
  constructor (name){
    super(name)
  }

  jump(){
    return "hop hop";
  }

} 

//Soal 2
class Clock {
    constructor(template){
      this.date = new Date();
      this.hours = this.date.getHours();
      this.mins = this.date.getMinutes();
      this.secs = this.date.getSeconds();
    }
  
    render() {
      if (this.hours < 10) 
      this.hours = '0' + this.hours;
  
      if (this.mins < 10) 
      this.mins = '0' + this.mins;
  
      if (this.secs < 10) 
      this.secs = '0' + this.secs;
     
      console.log(this.hours+":"+this.mins+":"+this.secs);
      
    }

    start(){
      this.render();
      setInterval(this.render.bind(this),1000);
    }
    
  }
    
    var clock = new Clock({template: 'h:m:s'});
    clock.start(); 