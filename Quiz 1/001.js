//Soal A
function bandingkan (a,b){
    if(a < 0|| b < 0){
      return -1;
    }else if( a == null && b == null){
      return -1;
    }else if( a != null && b == null){
      return a;
    }else{
      if(a>b){
        return a;
      }else if(a<b){
        return b;
      }else{
        return -1;
      }
    }
    }
    
    console.log(bandingkan(10, 15)); // 15
    console.log(bandingkan(12, 12)); // -1
    console.log(bandingkan(-1, 10)); // -1 
    console.log(bandingkan(112, 121));// 121
    console.log(bandingkan(1)); // 1
    console.log(bandingkan()); // -1
    console.log(bandingkan("15", "18")) // 18

//Soal B
function balikString(kata){
    var   kumpulanHuruf = kata;
    var arrayHuruf = [];
    var reverseArray= [];
    var newWord = "";
    for(i=0; i<kumpulanHuruf.length; i++){
      arrayHuruf.push(kumpulanHuruf[i]);
    }
    for(x=arrayHuruf.length-1; x>=0 ; x--){
      reverseArray.push(arrayHuruf[x]);
    }
    for(y=0; y<reverseArray.length; y++){
      newWord += reverseArray[y];
    }
      return newWord;
    }
    
  console.log(balikString("abcde")) // edcba
  console.log(balikString("rusak")) // kasur
  console.log(balikString("racecar")) // racecar
  console.log(balikString("haji")) // ijah

  //Soal C